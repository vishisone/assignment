import React, { useEffect,useState } from 'react'
import { Text, StyleSheet, View, FlatList,Image } from 'react-native'

export const UserList = () => {

    const [usersData,setUsersData] = useState([]);
    useEffect(()=> getUserList(),[]);

    const getUserList = () => {
        let apiUrl = `https://randomuser.me/api/?nat=us&results=20`;
        fetch(apiUrl).then(response => response.json()).then((data)=>{
            if(typeof data.results != "undefined"){
                setUsersData(data.results);
            }
        }).catch((e)=>console.log("error",e));
    }

    const userView = ({item,index}) => {
        const {title,first,last} = item.name;
        return (
            <View style={style.cardContainer}>
                <Image source={{ uri:item.picture.medium }} style={style.image} resizeMode="contain" />
                <Text>{`${title} ${first} ${last}`}</Text>
            </View>
        )
    }

    return (
        <>
            <Text>User list</Text>
            <FlatList
                data={usersData}
                keyExtractor={(item,index)=>`user_${index}`}
                renderItem={userView}
            />
        </>
    )

}


const style = StyleSheet.create({
    cardContainer:{
        alignItems:'center',
        paddingVertical:5,
        borderColor:"#aaa",
        borderWidth:1,
        marginVertical:5
    },
    image : {
        width:"80%",
        height:150
    }
})