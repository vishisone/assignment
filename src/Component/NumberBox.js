import React from 'react'
import { Button} from 'react-native'
import {Style} from '../Style/index';
export const NumberBox = (props) => {
   
    return (
        <Button 
            title={`${props.value}`} 
            style={{ borderColor:'#aaa',borderWidth:1,color:"#fff",backgroundColor:"purple"}} 
            onPress={()=>props.callback && props.callback(`${props.value}`)}
        />
    );
}

