import React,{useRef} from 'react';
import Video from 'react-native-video';
import {Style} from '../Style/index';
export const Post = (props)=> {
    // const playerRef = useRef();
    return (
        <Video 
            source={{uri: props.url}}  
            // ref={playerRef}                                      
            onBuffer={(buffer)=>console.log(buffer)}
            onError={(error)=>console.log(error)}  
            style={Style.backgroundVideo} 
            controls ={true}
            fullscreen={false}
            resizeMode="cover"
        />
    );
}

