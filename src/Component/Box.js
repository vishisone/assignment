import React,{useRef,useMemo,useState, useEffect} from 'react'
import { TextInput ,Keyboard} from 'react-native'

import {Style} from '../Style/index';

export const Box = (props) => {
    let {row,col} = props;
    const [value,setValue] = useState(null);

    const handleTouch = (txt) =>{
        let inputValue = value;
        if(typeof txt != "undefined"){
            inputValue = txt;
        }
        let data = {
            row : row,
            col : col,
            value : inputValue
        }
        props.callback && props.callback(data);
    }

    const showValue= () =>{
        let board = props.board && props.board;
        if(typeof board != "undefined" && board.length > 0){
            let rowData = board.find((item)=>item.row == row && item.col == col);
            if(rowData && Object.keys(rowData).length > 0){
                return ( rowData.value) ?  `${rowData.value}` : null;
            }
        }
        return null;
    }
    
    
    return (
        <TextInput 
            showSoftInputOnFocus={false} 
            keyboardType="numeric" 
            style={Style.boxContainer} 
            value={showValue()}
            onFocus={()=>handleTouch()}
            onChangeText={(text) => handleTouch(text)}
        />
    );
}

