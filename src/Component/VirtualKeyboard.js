import React, { Component,useReducer } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { NumberBox } from './NumberBox';
import {INITIAL_STATE, reducer} from '../Store/store';
import { useStateTheme, useUpdateStateTheme } from '../Board/StateProvider';
import { ACTION } from '../Store/store';
export const VirtualKeyboard = (props) =>{
    const {numberLength} = props;
    const state = useStateTheme();
    const updateState = useUpdateStateTheme();
    // const [state,dispatch] =  useReducer(reducer,INITIAL_STATE)
    let inputNumbersCount = [... Array(numberLength)];

    const handleCallback = (num) => {
        if(typeof state.active != "undefined" && (state.active).length > 0){
            let data = state.active;
            data.value = (data.value) ? data.value+`${num}` :`${num}`;
            updateState({type:ACTION.ACTIVE_INPUT,payLoad:data});
        }
    }

    return (
        <View style={{ 
            height:30,
            flexDirection:"row",
            justifyContent:"space-between",
            marginTop:10
         }}>
            {inputNumbersCount.map((e,i)=><NumberBox value={i} key={`key_${i}`} callback={handleCallback} />)}
        </View>
    )

}
