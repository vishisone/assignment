import React, { Component,useEffect,useReducer } from 'react'
import { Text, View } from 'react-native'
import { Box } from './Box';
import { useStateTheme, useUpdateStateTheme } from '../Board/StateProvider';
import { ACTION } from '../Store/store';

const RowBox = (props) => {
    const state = useStateTheme();
    const updateState = useUpdateStateTheme();
    const {col,rowNumber} = props;
    const handleCallback =(data) =>{
        updateState({type:ACTION.ACTIVE_INPUT,payLoad:data})
    }
    return (
        <View style={{ flexDirection:'row' }}>
            {[...Array(col)].map((e, i) => <Box row={rowNumber} {...state} col={i+1} key={`box_${rowNumber}_${i}`} callback={handleCallback} />)}
        </View>
    )

}

export default RowBox
