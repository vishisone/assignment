import React,{useState,useContext, useReducer} from 'react';
import { INITIAL_STATE, reducer } from '../Store/store';
const ThemeStore = React.createContext();
const ThemeStoreUpdate = React.createContext();

export const useStateTheme = () => {
    return useContext(ThemeStore);
}

export const useUpdateStateTheme = () => {
    return useContext(ThemeStoreUpdate);
}

export const StateProvider = ({children}) => {
    const [state,dispatch] = useReducer(reducer,INITIAL_STATE);

    const updateState = (data) => {
        dispatch(data);
    }
    
    return (
        <ThemeStore.Provider value={state}>
            <ThemeStoreUpdate.Provider value={updateState}>
                {children}
            </ThemeStoreUpdate.Provider>    
        </ThemeStore.Provider>
    );

}
