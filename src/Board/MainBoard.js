import React,{useReducer} from 'react';
import { Button, View } from 'react-native';
import RowBox from '../Component/RowBox';
import { VirtualKeyboard } from '../Component/VirtualKeyboard';
import { useStateTheme, useUpdateStateTheme } from '../Board/StateProvider';
import { ACTION } from '../Store/store';
import { NumberBox } from '../Component/NumberBox';
const TABLE_BOX = 6;
const KEYBOARD_BOX = 10;

const MainBoard = (props) => {
    let boxLength = [...Array(TABLE_BOX)];
    let inputBtnLength = [...Array(KEYBOARD_BOX)];
    const updateState = useUpdateStateTheme();
    const state = useStateTheme();

    const handleCallback = (num) => {
        if(state && typeof state.active != "undefined"){
            let activeData = state.active;
            if(Object.keys(activeData).length > 0){
                let data = activeData;
                data.value = (data.value) ? data.value+`${num}` :`${num}`;
                updateState({type:ACTION.ACTIVE_INPUT,payLoad:data});
            }
        }
    }

    const handleSubmit = () =>{
        updateState({type:ACTION.SUBMIT_BOARD});
    }

    let {questionOutput} = state;
    return (
        <>
            <View style={{ flex:2,justifyContent:"space-between",alignSelf:'center',marginVertical:10 }}>
                {boxLength.map((e,i) => <RowBox key={`board_${i}`} rowNumber={i+1} col={TABLE_BOX} />)}
            </View>
            <View style={{ flex:1,justifyContent:'space-around', }}> 
                <View style={{ height:30,flexDirection:"row",justifyContent:"space-around",marginTop:10}}>
                    {inputBtnLength.map((e,i)=><NumberBox value={i} key={`key_${i}`} callback={handleCallback} />)}
                </View>
                { typeof questionOutput != "undefined" && questionOutput.length > 0 && 
                    <Button title="View Json" onPress={()=>alert(JSON.stringify(questionOutput))} />
                }

                { typeof questionOutput == "undefined" || questionOutput.length < 1 && 
                    <Button title="Next" onPress={handleSubmit} />
                }

            </View>
        </>
    );
}

export default MainBoard;
