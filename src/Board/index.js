import React,{useReducer} from 'react';
import { StateProvider } from './StateProvider';
import MainBoard from './MainBoard';
const Board = (props) => {
    return (
        <StateProvider>
            <MainBoard />
        </StateProvider>
    );
}

export default Board;
