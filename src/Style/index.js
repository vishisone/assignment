import { StyleSheet,Dimensions } from 'react-native';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export const Style =  StyleSheet.create({
    backgroundVideo: {
        height:screenHeight/3.5,
        width :screenWidth-30,
        marginHorizontal : 15,
        marginVertical:8,
        borderRadius:5,
        borderWidth:2,
        borderColor:"#000",
        elevation:5,
    },
    boxContainer:{
        borderColor:"#aaa",
        borderWidth:1,
        maxHeight:(screenHeight/10)+5,
        maxWidth:screenWidth/6,
        minHeight:(screenHeight/10)+5,
        minWidth:screenWidth/6,
    }
});

