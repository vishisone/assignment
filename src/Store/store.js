import {Children, useReducer} from 'react';
export const INITIAL_STATE = {active:undefined,board:[],questionOutput:[]};

export const ACTION = {
    ACTIVE_INPUT : 'ACTIVE_INPUT',
    SUBMIT_BOARD : 'SUBMIT_BOARD'
}

export function reducer(state,action){
    switch(action.type){
        case ACTION.ACTIVE_INPUT:
            let {active,board} = state;
            let data = action.payLoad;
            if(board.length > 0){
                let findActiveState = board.findIndex((item)=>item.row == data.row && item.col == data.col);
                if(findActiveState !== -1){
                    board  = board.map((item)=>{
                        if(item.row == data.row && item.col == data.col){
                            item.value = data.value;
                        }
                    });
                }else{
                    board.push(data);
                }
                state.board;
            }else{
                board.push(data);
                state.board = board;
            }
            state.active = data;
            state.questionOutput = [];
            return {...state};
        case ACTION.SUBMIT_BOARD :
            return {active:undefined,board:[],questionOutput:state.board}
        default : 
            return state;
    }
}
