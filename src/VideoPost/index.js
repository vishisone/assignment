import React ,{useRef,useEffect} from 'react';
import {FlatList} from 'react-native';
import { Post } from '../Component/Post';

const VIDEOS = [
    "https://content.jwplatform.com/manifests/yp34SRmf.m3u8",
    "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8",
    "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
];

const VideoPost = ()  => {
    return (
        <>
            <FlatList
                data={VIDEOS}
                renderItem={({item,index})=><Post key={index} url={item} />}
                keyExtractor={(item,index) => `video_${index}`}
            />
        </>
        
    );
}

export default VideoPost;
