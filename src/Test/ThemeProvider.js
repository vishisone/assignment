import React,{useState,useContext} from 'react';
const ThemeContext = React.createContext();
const ThemeUpdate = React.createContext();

export const useTheme = () => {
    return useContext(ThemeContext);
}

export const useUpdateTheme = () => {
    return useContext(ThemeUpdate);
}

export const ThemeProvider = ({children}) => {
    const [theme,ChangeTheme] = useState(true);

    const toggleTheme = () => {
        ChangeTheme(prevTheme => !prevTheme);
    }
    
    return (
        <ThemeContext.Provider value={theme}>
            <ThemeUpdate.Provider value={toggleTheme}>
                {children}
            </ThemeUpdate.Provider>    
        </ThemeContext.Provider>
    );

}


