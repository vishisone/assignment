import React, { Component,useMemo,useState,useCallback,useEffect,useRef,useReducer} from 'react';
import { View, Text,Button } from 'react-native';
const calculate = (number) => {
    // for(let i = 0; i <=100000000;i++){
    //     // console.log(i)
    // }
    return number *2;
}
const ACTION = {
    INCREMENT : "INCREMENT",
    DECREMENT : "DECREMENT",
}


function reducer(state,action){
    switch(action.type){
        case ACTION.INCREMENT : 
            return {count : state.count + 1};
        case ACTION.DECREMENT :     
            return {count : state.count - 1};
        default :
            return state;
    }
}

export const Main  = () =>{

    const [number,setNumber] = useState(0)
    const [themeMode,changeTheme] = useState(false)
    const [state,dispatch] = useReducer(reducer,{count : 0})
    const viewRef = useRef(0);
    useEffect(() => {
        viewRef.current = viewRef.current + 1;
        // return () => {
        //     console.log("Unmount");
        // }
    }, [state])

    let finalValue = useMemo(()=>{
        return calculate(number)
    },[number,theme]);

    let theme = {
        backgroundColor : themeMode ? "purple" : "pink",
        flex:1
    };

    let arr = ["vijay","ajay","rohan","shital","vijay","sohan","ravi","sohan","ajay",{hello:"tree"}];
    let arr1 = [{hello:"tree"}];
    let newArray = [];
    // console.log(arr);
    // console.log(arr.reverse());
    console.log(Array.isArray(arr1));
    console.log(typeof arr1)
    let temp = "";
    // arr.forEach((item,index)=>{
    //     // console.log(newArray.lastIndexOf(item));
    //     // console.log(newArray.find(data => data === item));
    //     // console.log(newArray.findIndex(data => data === item));
    //     // console.log(newArray.filter(data => data === item));
    //     if(newArray.indexOf(item) === -1){
    //         newArray.push(item);
    //     }
    // })
    // let length = arr.length;
    // let temp = "Hello world";
    // for(let a= 0; a < length; a++ ){
    //     if(temp != arr[a]){
    //         temp = arr[a];
    //         newArray.push(arr[a]);
    //     }
    // }
    // console.log(a);
    // console.log(newArray);
    // console.log(`Normal Array Element = ${arr.length}`);
    // console.log(arr);
    // let result = arr.slice(3,6);
    // console.log(result);
    // console.log(`After Array Element = ${result.length}`);
    // let uniqueArray = arr.reduce((unique,item)=>(!unique.includes(item)) ? unique.concat(item) : unique,[]);
    // console.log(uniqueArray);

    return (
        <>
            <View style={theme}>
                <Text>{`Current Number= ${state.count}`}</Text>
                <Text>{`Component Renter Time = ${viewRef.current}`}</Text>
                <Text>{`Calculated value = ${finalValue}`}</Text>
                <Button title="Increase" onPress={()=>{
                    dispatch({type : ACTION.INCREMENT});
                    //setNumber(number+1)
                 }} />
                <Button title="Decrease" onPress={()=>{
                     dispatch({type : ACTION.DECREMENT})
                    // setNumber(number-1)
                    }} />
                <Button title="Change Theme" onPress={()=>changeTheme(oldChangeTheme=>!oldChangeTheme)} />
            </View>
        </>
    )
}


