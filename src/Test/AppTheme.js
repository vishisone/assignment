import React,{useState} from 'react';
import { ClassApp } from './ClassApp';
import { FunctionApp } from './FunctionApp';
import {ThemeProvider} from './ThemeProvider';

export const AppTheme = () => {

    return(
        <ThemeProvider>
            <ClassApp />
            <FunctionApp />
        </ThemeProvider>
    )
};
