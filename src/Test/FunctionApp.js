import React from 'react';
import { View,Text, Button } from 'react-native';
import {useTheme,useUpdateTheme} from './ThemeProvider';

export const FunctionApp = () => {
    const theme = useTheme();
    const updateTheme = useUpdateTheme();
    let style = {
        backgroundColor :theme ? "#000" : "#eee", 
        flex:1,
        alignItems:"center",
        flexDirection:"row",
        justifyContent:"center" ,
        margin : 5,
        borderColor: "red",
        borderWidth:2,
        borderRadius:10,
        elevation:100
    }
    return(
        <View style={style}>
            <Text>Functional Magic Component </Text>
            <Button title ="Update Theme" onPress={updateTheme} />
        </View>
    )
};
